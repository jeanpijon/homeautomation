import json
import threading
import time
from threading import Thread
import logging
logging.basicConfig(level=logging.INFO)

import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import paho.mqtt.client as mqtt_client

class Shade(Thread):
    def __init__(self, shade_name, shade_remote, shade_channel, mqtt_broker='localhost'):
        super().__init__()
        self.shade_id = f"{shade_remote}_{shade_channel}"
        self.logger = logging.getLogger(self.__class__.__name__ + self.shade_id)
        self.shade_remote = shade_remote
        self.shade_name = shade_name
        self.shade_channel = shade_channel
        self.connected = False
        self.mqtt_broker = mqtt_broker
        self.action_updated = threading.Event()

        self.position = 0
        self.action = "STOP"
        self.action_time = None
        self.max_time_to_complete = 5

        self.command_topic_prefix = f"home-assistant/mqtt-covers"
        self.position_topic = f"home-assistant/cover{self.shade_id}/position"
        self.client = None
        self.connect()
        #print(f"Init: {self.shade_id}")

    def connect(self):
        self.client = mqtt_client.Client(f"shade_{self.shade_id}")
        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_message = self.on_message
        self.client.connect(self.mqtt_broker)  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        return True

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")

    def run(self):
        self.register()
        self.logger.info(f"Starting: {self.shade_id}")
        remaining_time_to_complete = self.max_time_to_complete
        poll_interval = 1
        previous_action = self.action
        action_time = time.time_ns()
        while True:
            if self.action_updated.wait(poll_interval):
                action_duration = (time.time_ns() - action_time)/1000/1000/1000
            else:
                action_duration = poll_interval

            done_chunk = (action_duration/self.max_time_to_complete)*100
            self.logger.info(
                f"previous:{previous_action} actual:{self.action} remaining_time: {remaining_time_to_complete} position:{self.position} action_duration:{action_duration}, done:{done_chunk}%")
            #self.logger.info(f"{remaining_time_to_complete} {action_duration}, {done_chunk}")
            if previous_action != self.action: # we have edge
                if self.action == "OPEN":  # new action is open
                    if previous_action == "STOP": #start from stop status
                        pass
                    elif previous_action == "CLOSE":  # start from stop status
                        pass
                if self.action == "CLOSE":  # new action is open
                    if previous_action == "STOP": #start from stop status
                        action_time = self.action_time
                    elif previous_action == "OPEN": #start from stop status
                        pass
            else: #same old, same old
                if previous_action == "OPEN":
                    remaining_time_to_complete -= action_duration

                    self.position += done_chunk
                    if self.position >= 100:
                        self.action = "STOP"
                        self.position = 100
                elif previous_action == "CLOSE":
                    remaining_time_to_complete -= action_duration
                    self.position -= done_chunk
                    if self.position <= 0:
                        self.action = "STOP"
                        self.position = 0
                else:
                    #this is infinite wait till the action comes
                    self.logger.info("Setting poll interval to very big number")
                    poll_interval = 10e6

            previous_action = self.action #set current to previous
            self.logger.info(
                f"previous:{previous_action} actual:{self.action} remaining_time: {remaining_time_to_complete} position:{self.position} action_duration:{action_duration}, done:{done_chunk}%")
            self.action_updated.clear()
            # if self.action == "STOP": #current action is stop
            #     self.logger.info("Received new command STOP")
            #     if previous_action == "OPEN": #previous was OPEN
            #         new_position = self.position + done_factor
            #         self.position = 100 if new_position > 100 else new_position
            #     elif previous_action == "CLOSE":
            #         new_position = self.position - done_factor
            #         self.position = 0 if new_position < 0 else new_position
            #     elif previous_action == "STOP":
            #         pass
            # elif self.action == "OPEN": #current action is open
            #     if previous_action == "STOP": #start from stop status
            #         pass
            #     elif previous_action == "CLOSE":  # start from stop status
            #         pass
            #     elif previous_action == "OPEN": #start from stop status
            #         pass
            # elif self.action == "CLOSE":
            #     self.logger.warning("Received new command CLOSE- NOT IMPLEMENTED")
            #     pass
            # remaining_time_to_complete = remaining_time_to_complete-action_duration
            # previous_action = self.action
            # action_time = self.action_time
            #
            # #timer expired
            # self.client.publish(self.position_topic, self.position)
            # #publish.single(self.position_topic, self.position, hostname=self.mqtt_broker)
            # self.action = "STOP"
            # self.action_updated.clear()

    def on_message(self, client, userdata, message):
        #print("%s: %s %s" % (self.shade_id, message.topic, message.payload))
        action, remote, channel = message.payload.decode('utf-8').split("_")
        #print(action, remote, channel)
        if int(remote) == self.shade_remote:
            if int(channel) == self.shade_channel or int(channel) == 0:
                print(self.shade_id, action, remote, channel)
                if action != self.action:
                    self.action_time = time.time_ns()
                    self.action = action
                    self.action_updated.set()

                    #publish.single(self.position_topic, "100", hostname=self.mqtt_broker)
                    #publish.single(self.position_topic, "0", hostname=self.mqtt_broker)


    def register(self):
        topic = f"homeassistant/cover/shade{self.shade_id}/config"
        payload = {
            "name": f"{self.shade_name}",
            "command_topic": f"{self.command_topic_prefix}/set",
            "position_topic": self.position_topic,
            #"set_position_topic": f"home-assistant/cover{self.shade_id}/set_position",
            "retain": True,
            "position_open": 100,
            "position_closed": 0,
            "payload_stop": f"STOP_{self.shade_id}",
            "payload_open": f"OPEN_{self.shade_id}",
            "payload_close": f"CLOSE_{self.shade_id}",
            "optimistic": False,
            "unique_id": f"coverX{self.shade_id}",
            "device": {
                "identifiers": f"testing.cov.{self.shade_id}"
            },
            "device_class": "shade"
        }
        self.logger.info(f"Registering: {self.shade_name}")
        self.client.publish(topic, json.dumps(payload))
        self.client.subscribe(f"{self.command_topic_prefix}/set-rf")

shade_config = {"First": [670, 0]}


s = Shade("Shades Living Room East", 670, 1)
#s2 = Shade("Shades Living Room West", 670, 2)
s.start()
#s2.start()
s.join()
#s2.join()
#     time.sleep(5)
#     publish.single('house/shades/shade1', "STOP")
#     time.sleep(5)
