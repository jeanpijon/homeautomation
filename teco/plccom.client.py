from telnetlib import Telnet

tn = Telnet('192.168.10.174', 5010)
tn.write("LIST:".encode('ascii') + b"\n")
print(tn.read_until(b"LIST:").decode('ascii'))


tn.write("GET:MAIN.ROOM1.TIMEPRG.TIMEPROGCONTROL.GTSAP1_TPW_ROOMTEMP".encode('ascii') + b"\n")
print(tn.read_some().decode('ascii'))

import asyncio, telnetlib3

@asyncio.coroutine
def shell(reader, writer):
    print("Running Client")
    buf = ''
    while True:
        outp = yield from reader.read(1024)
        if not outp:
            # EOF
            return

        print(outp, end='', flush=True)

        if '?' in outp:
            # reply all questions with 'y'.
            writer.write('y')

loop = asyncio.get_event_loop()
coro = telnetlib3.open_connection('192.168.10.174', 5010, shell=shell, encoding='ascii')
reader, writer = loop.run_until_complete(coro)
loop.run_until_complete(writer.protocol.waiter_closed)