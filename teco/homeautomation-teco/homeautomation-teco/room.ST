TYPE
  TRelayArray : ARRAY[1..11] OF BOOL;
END_TYPE

FUNCTION_BLOCK fbRoom
  VAR_INPUT
           name     : string[80];
           CO2_in   : real := -1;
  END_VAR
  VAR_IN_OUT
      Thermo_in             : TCIB_C_RC_0011R_IN;  (*vstupn� data z modulu C-RC-0011R*)
      Thermo_out            : TCIB_C_RC_0011R_OUT;  (*v�stupn� data pro modul C-RC-0011R*)
      HEATING_RELAY   {PUBLIC_IFOX}      : BOOL;
      HEATING_RELAY_1 {PUBLIC_IFOX}      : BOOL;
      HEATING_RELAY_2 {PUBLIC_IFOX}      : BOOL;
      HEATING_RELAY_3 {PUBLIC_IFOX}      : BOOL;
      HEATER               : BOOL;
      COOLER              : BOOL;
      Fan_in          : USINT;

  END_VAR
  VAR
      Thermostat                                : fb_iTimeProgWeek;
      timePrgSettings                           : T_TIME_PROG_WEEK_INTERFACE;
      webSettingsEng                            : T_TIME_PROG_WEEK_WEB_SETTINGS_ENG := ( darkBg  := TRUE);
      SENSOR_TEMPERATURE_FLOOR  {PUBLIC_IFOX}   : REAL;
      SENSOR_TEMPERATURE_AIR  {PUBLIC_IFOX}     : REAL;
      SENSOR_HUMIDITY_AIR {PUBLIC_IFOX}         : USINT;
      SENSOR_CO2_AIR {PUBLIC_IFOX}              : REAL;
      heatTemp                                  : REAL;
      FAN       {PUBLIC_IFOX}         : USINT;
      ACTION  {PUBLIC_IFOX}  : string[80];
  END_VAR
  VAR_TEMP
  END_VAR

  SENSOR_TEMPERATURE_FLOOR  :=      Thermo_in.eTHERM;
  SENSOR_TEMPERATURE_AIR    :=      Thermo_in.iTHERM_iRH.iTHERM;
  SENSOR_HUMIDITY_AIR       :=      Thermo_in.iTHERM_iRH.iRH;
  SENSOR_CO2_AIR            :=      CO2_in;
  Thermo_out.VAL.DATA[3]    :=      USINT_TO_INT(Thermo_in.iTHERM_iRH.iRH);
  Thermo_out.VAL.DATA_TYPE[3]    := 4;
  
  timePrgSettings.stat.units := 1;

  Thermostat(
           roomTemp             := Thermo_in.iTHERM_iRH.iTHERM,
           hyst                 := 0.2,
           typ                  := ICL_TPW_TYPE_HEATING_COOLING,
           numTimes             := 5,
           name                 := name,
           TimeProg_Settings    := timePrgSettings,
           TimeProg_webSettings := webSettingsEng,
           C_RC_in              := Thermo_in,
           C_RC_out             := Thermo_out,
           heat                 => HEATER,
           cool                 => COOLER,
           heatTemp             => heatTemp
           );

  Thermo_out.ICO.COOLING := COOLER;
  Thermo_out.ICO.HEATING := HEATER;

  HEATING_RELAY := HEATER;
  HEATING_RELAY_1 := HEATER;
  HEATING_RELAY_2 := HEATER;
  HEATING_RELAY_3 := HEATER;
  IF Fan_in = 0 THEN
     Thermo_out.ICO.FAN_0 := TRUE;
     Thermo_out.ICO.FAN_1 := FALSE;
     Thermo_out.ICO.FAN_2 := FALSE;
     Thermo_out.ICO.FAN_3 := FALSE;
  ELSIF Fan_in = 1 THEN
     Thermo_out.ICO.FAN_0 := TRUE;
     Thermo_out.ICO.FAN_1 := TRUE;
     Thermo_out.ICO.FAN_2 := FALSE;
     Thermo_out.ICO.FAN_3 := FALSE;
  ELSIF Fan_in = 2 THEN
     Thermo_out.ICO.FAN_0 := TRUE;
     Thermo_out.ICO.FAN_1 := TRUE;
     Thermo_out.ICO.FAN_2 := TRUE;
     Thermo_out.ICO.FAN_3 := FALSE;
  ELSIF Fan_in = 3 THEN
     Thermo_out.ICO.FAN_0 := TRUE;
     Thermo_out.ICO.FAN_1 := TRUE;
     Thermo_out.ICO.FAN_2 := TRUE;
     Thermo_out.ICO.FAN_3 := TRUE;
  END_IF;

  IF COOLER THEN
     ACTION := 'cooling';
  ELSIF HEATER THEN
     ACTION := 'heating';
  ELSE
     ACTION := 'idle';
  END_IF;


END_FUNCTION_BLOCK

