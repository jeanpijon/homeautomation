VAR_GLOBAL

END_VAR



PROGRAM prgMain
  VAR_INPUT
  END_VAR
  VAR_OUTPUT
  END_VAR
  VAR

    MQTTControl2          : fbMQTTPublisher;
    MQTTControlSubs       : fbMQTTSubscriber;
    brokerIPaddr          : STRING := '192.168.0.10';//IP MQTT brokeru
    remotePort            : UINT := 1883;
    localPort             : UINT := 60000;
    keepAlive             : bool := true;
    keepAliveInterval     : time := T#60s;
    pingInterval          : time := T#10s;
    connTimeOut           : time := T#10s;
    connect               : bool := true;



    loginName             : string[32] := 'test';
    loginPass             : string[32] := 'test';

    connTimeOutSubs       : time := T#10s;
    connectSubs           : bool := true;
    localPortSubs         : UINT := 50000;
    keepAliveSubs         : bool := true;
    keepAliveIntervalSubs : time := T#60s;
    com_param             : T_MQTT_COM_PARAM;


    subDataTxt            : string[255];
    subTopicTxt           : string[80]  := 'house/room1';

    sendCom               : bool;

    pubDataTxt            : string[255] := '"{"light_1": "on","temp_1": 24.2}"';
    pubTopicTxt           : string[80]  := 'house/room1';

    pubSendCom            : bool;


  END_VAR
  VAR_TEMP
  END_VAR

     MQTTControl2(chanCode         := ETH1_UNI0,
                 brokerIP          := STRING_TO_IPADR(brokerIPaddr),
                 brokerPort        := MQTT_COMM_PORT,
                 localPort         := localPort,
                 connect           := connect,
                 keepAlive         := keepAlive,
                 keepAliveInterval := keepAliveInterval,
                 pingInterval      := pingInterval,
                 connTimeOut       := connTimeOut,
                 com_param         := com_param,
                 loginName         := loginName,
                 loginPass         := loginPass,
                 dataTxt           := pubDataTxt,
                 topicTxt          := pubTopicTxt
                );


    MQTTControlSubs(chanCode          := ETH1_UNI1,
                    brokerIP          := STRING_TO_IPADR(brokerIPaddr),
                    brokerPort        := MQTT_COMM_PORT,
                    localPort         := localPortSubs,
                    connect           := connectSubs,
                    keepAlive         := keepAliveSubs,
                    keepAliveInterval := keepAliveIntervalSubs,
                    pingInterval      := pingInterval,
                    connTimeOut       := connTimeOutSubs,
                    com_param         := com_param,
                    loginName         := loginName,
                    loginPass         := loginPass,
                    subTopicTxt       := subTopicTxt
                   );



END_PROGRAM
