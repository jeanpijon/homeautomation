import signal


import logging
import argparse

from heat_pump.mqtt_bridge import HeatPumpMQTTAdapter


def main(args):
    logging.basicConfig(level=args.verbose)

    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

    signal.signal(signal.SIGINT, original_sigint_handler)
    # read config
    # instantiate mqtt bridge instances for shades
    # instantiate RF transmitter
    # instantiate RF receiver


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MQTT bridge for controlling Spirala Heat Pump')
    parser.add_argument('--config', action='store', type=str, default="/opt/homeassistant-dev/services/config/heat_pump/config.yaml")
    parser.add_argument('--verbose', action='store', type=int, default=0)

    args, unknown = parser.parse_known_args()
    main(args)