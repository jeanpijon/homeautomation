import json
import logging
import time
from threading import Thread
import queue

import pigpio
import yaml

from rx import RX

logging.basicConfig(level=logging.INFO)

import paho.mqtt.client as mqtt_client
from enum import Enum


class FSMStates(Enum):
    IDLE = 1
    OPENING = 2
    CLOSING = 3


class ShadeController(Thread):
    def __init__(self, shade_id, shade_config, mqtt_config, rf_config):
        super().__init__()
        self.shade_id = shade_id
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.shade_id}")
        self.logger.info(f"Initializing")
        self.logger.info(shade_config)
        self.shade_config = shade_config
        self.connected = False
        self.mqtt_config = mqtt_config
        self.rf_config = rf_config
        self.client = None
        self.position_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/position"
        self.command_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/set"
        self.timeout = 1
        self.queue = queue.Queue()
        self.fsm_state = FSMStates.IDLE

    def connect(self):
        self.client = mqtt_client.Client(f"shade_{self.shade_id}")
        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_message = self.on_message
        self.client.connect(self.mqtt_config['host'])  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        return True

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")

    def run(self):
        self.connect()
        self.register()
        self.logger.info(f"Starting: {self.shade_id}")
        timeout = 0
        while True:
            try:
                command = self.queue.get(timeout=timeout)
            except queue.Empty:
                command = "NOP"

            if self.fsm_state == FSMStates.IDLE:
                if command == "STOP":
                    # self.client.publish(self.position_topic, 50)
                    self.logger.info(f"Stop in IDLE, nothing to be done")
                elif command == "OPEN":
                    self.logger.info(f"Set OPEN")
                    if self.position < self.shade_config['dark']:
                        pass
                    else:
                        pass
                    self.client.publish(self.position_topic, 100)
                elif command == "CLOSE":
                    self.logger.info(f"Set CLOSE")
                    self.client.publish(self.position_topic, 0)
                elif command == "NOP":
                    pass

            elif self.fsm_state == FSMStates.OPENING:
                pass
            elif self.fsm_state == FSMStates.CLOSING:
                pass

    def on_message(self, client, userdata, message):
        # print("%s: %s %s" % (self.shade_id, message.topic, message.payload))
        action = message.payload.decode('utf-8')
        topic = message.topic
        shade_id = topic.split('/')[-2]

    def register(self):
        topic = f"homeassistant/cover/shade{self.shade_id}/config"
        payload = {
            "name": f"{self.shade_config['name']}",
            "command_topic": self.command_topic,
            "position_topic": self.position_topic,
            "position_open": self.shade_config['dark'],
            "position_closed": 0,
            "retain": True,
            "optimistic": True,
            "unique_id": f"{self.shade_id}.cover",
            "device": {
                "identifiers": f"rc.{self.shade_id}"
            },
            "device_class": "shade"
        }
        self.logger.info(f"Registering: {self.shade_id}")
        self.client.publish(topic, json.dumps(payload))


actions = {
    0b0001000: "OPEN",  # up_start
    0b0001111: "UP_STOP",  # up_stop
    0b0101010: "STOP",  # stop
    0b0011001: "CLOSE",  # down_start
    0b0011110: "DOWN_STOP",  # "down_stop" : 0b0011110, #down_stop
    0b1100110: "PAIR"  # pair
}





if __name__ == '__main__':
    config_file = '/config/config.yaml'
    with open(config_file, 'r') as fp:
        config = yaml.load(fp, Loader=yaml.FullLoader)
    rx_queue = queue.Queue()
    controllers = {}
    for shade_id, shade_config in config['shades'].items():
        remote_inverse_id = config['rf']['remotes'][shade_config['remote']]
        shade_inverse_id = f"{remote_inverse_id}_{shade_config['channel']}"
        controllers[shade_inverse_id] = ShadeController(shade_id, shade_config, config['mqtt'], config['rf'])

    # controllers = {f"{shade_config['']}": ShadeController(shade_id, shade_config, config['mqtt'], config['rf']) for shade_id, shade_config in config['shades'].items()}
    for shade_id, controller in controllers.items():
        controller.start()


    RX_port = 27
    print(controllers.keys())
    rxc = RX(config=config, q=rx_queue)

    for shade_id, controller in controllers.items():
        controller.join()

    rxc.cancel()  # Cancel the receiver.



    logging.info("Exiting")
