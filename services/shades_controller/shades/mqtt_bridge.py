import json
import logging
import time
import queue

import paho.mqtt.client as mqtt_client
import yaml

from heat_pump.controller import HeatPump


class BaseMQTTAdapter():
    def __init__(self, config_path):
        super().__init__()
        with open(config_path) as c:
            self.config = yaml.load(c, Loader=yaml.FullLoader)
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.config['id']}")
        self.connected = False
        self.mqtt_config = self.config['mqtt']
        self.client = None
        self.timeout = 1
        self.device_id = self.config['id']
        self.client = mqtt_client.Client(f"{self.device_id}-{time.time()}")

    # def register(self):
    #     topic = f"homeassistant/cover/shade{self.shade_id}/config"
    #     payload = {
    #         "name": f"{self.shade_config['name']}",
    #         "command_topic": self.command_topic,
    #         "position_topic": self.position_topic,
    #         "position_open": self.shade_config['dark'],
    #         "position_closed": 0,
    #         "retain": True,
    #         "optimistic": True,
    #         "unique_id": f"{self.shade_id}.cover",
    #         "device": {
    #             "identifiers": f"rc.{self.shade_id}"
    #         },
    #         "device_class": "shade"
    #     }
    #     self.logger.info(f"Registering: {self.shade_id}")
    #     self.client.publish(topic, json.dumps(payload))
    def connect(self):

        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.connect(self.mqtt_config['host'])  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        return True

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")


class MQTTSensor:
    def __init__(self, name, client, device, device_class, unique_id, unit=None):
        self.name = name
        self.client = client
        self.unit = unit
        self.device = device
        self.device["identifiers"] = unique_id
        self.unique_id = unique_id
        self.device_class = device_class
        self.value = None
        self.topic_prefix = f"homeassistant/sensor/{name}"
        self.reannounce_after = 15
        self.current_iteration = 0
        self.register()

    def register(self):
        config = {
            "~": self.topic_prefix,
            "name": self.name,
            "state_topic": "~/state",
            "unique_id": self.unique_id,
            "device": self.device
        }
        if self.unit is not None:
             config["unit_of_measurement"] = self.unit
        if self.device_class != "none":
            config["device_class"] = self.device_class
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=False)

    def __call__(self, value):
        if value != self.value or self.current_iteration > self.reannounce_after:
            self.client.publish(f"{self.topic_prefix}/state", payload=value, qos=0, retain=False)
            self.value = value
            self.current_iteration = 0
        else:
            self.current_iteration += 1


class HeatPumpMQTTAdapter(BaseMQTTAdapter):
    def __init__(self, config_path):
        super().__init__(config_path)
        self.config_path = config_path
        self.run_allowed = True
        self.q = queue.Queue()
        self.device = {
            "manufacturer": "Spirala",
            "suggested_area": "technicka-mistnost",
            "identifiers": "heat-pump-spirala"
        }
        self.state = {"mode": None, "fan": None}
        self.sensors = {}
        self.initialize()

    def initialize(self):
        self.topic_prefix = f"homeassistant/climate/{self.config['id']}"
        self.configuration_topic = f"{self.topic_prefix}/config"
        self.heat_pump = HeatPump(self.config_path, self.q,,
        self.client.message_callback_add(f"{self.topic_prefix}/mode/set", self.mode_callback)
        self.connect()

    def run(self):
        self.logger.info(f"Starting")
        self.heat_pump.start()
        self.client.subscribe(f"{self.topic_prefix}/#")
        config = {
            "~": self.topic_prefix,
            "name": self.config['name'],
            "mode_cmd_t": "~/mode/set",
            "mode_stat_t": "~/mode/state",
            "modes": ["off", "heat", "cool"],
            "unique_id": "venus-heat-exchanger-1",
            "device": self.device
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=False)
        while self.run_allowed:
            payload = self.q.get()
            if "state" in payload.keys() and self.state != payload["state"]:
                self.client.publish(f"{self.topic_prefix}/mode/state", payload=payload["state"], qos=0, retain=False)
                self.state = payload["state"]
            elif "sensors" in payload.keys():
                for sensor_name, sensor_data in payload["sensors"].items():
                    if sensor_name not in self.sensors.keys():
                        new_sensor = MQTTSensor(f"{self.config['id']}_{sensor_name}",
                                                self.client, self.device, sensor_data['device_class'],
                                                f"{self.config['id']}_{sensor_name}",
                                                unit=sensor_data['unit'])
                        self.sensors[sensor_name] = new_sensor
                    self.sensors[sensor_name](sensor_data['value'])

        print("Requesting stop on Heat Pump")
        self.heat_pump.stop = True
        self.heat_pump.join(60)

    def stop(self):
        self.logger.info(f"Stop requested")
        self.run_allowed = False

    def mode_callback(self, client, userdata, message):
        self.logger.info(f"Switching to mode {message.payload}")
        mode = message.payload.decode("utf-8")
        if mode == "off":
            mode = "idle"
            self.heat_pump.fan_speed = 0
        self.heat_pump.mode = mode
