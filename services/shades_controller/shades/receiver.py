import json
import logging
import time
from threading import Thread
import queue

import pigpio
import yaml

from rx import RX

logging.basicConfig(level=logging.INFO)

import paho.mqtt.client as mqtt_client
from enum import Enum


class FSMStates(Enum):
    IDLE = 1
    OPENING = 2
    CLOSING = 3


class ShadeController(Thread):
    def __init__(self, shade_id, shade_config, mqtt_config, rf_config):
        super().__init__()
        self.shade_id = shade_id
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.shade_id}")
        self.logger.info(f"Initializing")
        self.logger.info(shade_config)
        self.shade_config = shade_config
        self.rf_config = rf_config
        self.timeout = 1
        self.queue = queue.Queue()
        self.fsm_state = FSMStates.IDLE

    def run(self):

        self.logger.info(f"Starting: {self.shade_id}")
        timeout = 0
        while True:
            try:
                command = self.queue.get(timeout=timeout)
            except queue.Empty:
                command = "NOP"

            if self.fsm_state == FSMStates.IDLE:
                if command == "STOP":
                    # self.client.publish(self.position_topic, 50)
                    self.logger.info(f"Stop in IDLE, nothing to be done")
                elif command == "OPEN":
                    self.logger.info(f"Set OPEN")
                    if self.position < self.shade_config['dark']:
                        pass
                    else:
                        pass
                    self.client.publish(self.position_topic, 100)
                elif command == "CLOSE":
                    self.logger.info(f"Set CLOSE")
                    self.client.publish(self.position_topic, 0)
                elif command == "NOP":
                    pass

            elif self.fsm_state == FSMStates.OPENING:
                pass
            elif self.fsm_state == FSMStates.CLOSING:
                pass


actions = {
    0b0001000: "OPEN",  # up_start
    0b0001111: "UP_STOP",  # up_stop
    0b0101010: "STOP",  # stop
    0b0011001: "CLOSE",  # down_start
    0b0011110: "DOWN_STOP",  # "down_stop" : 0b0011110, #down_stop
    0b1100110: "PAIR"  # pair
}


def decode(code, check, counter, controllers):
    bin_code = bin(code)
    if len(bin_code) == 40:
        if bin_code[-3] == str(check):
            channel_id = int(bin_code[30:33], 2)
            remote_id = int(bin_code[19:30], 2)
            action = actions[int(bin_code[-7:], 2)]
            if action in ["CLOSE", "STOP", "OPEN"]:
                controller_id = f"{remote_id}_{channel_id}"
                if controller_id in controllers:
                    controllers[controller_id].queue.put(action)
                else:
                    logging.warning(f"Unknown controller ID: {controller_id}")
        else:
            print(f"Wrong checksum {bin_code[-3]} != {check}")


if __name__ == '__main__':
    config_file = '/config/config.yaml'
    with open(config_file, 'r') as fp:
        config = yaml.load(fp, Loader=yaml.FullLoader)

    controllers = {}
    for shade_id, shade_config in config['shades'].items():
        remote_inverse_id = config['rf']['remotes'][shade_config['remote']]
        shade_inverse_id = f"{remote_inverse_id}_{shade_config['channel']}"
        controllers[shade_inverse_id] = ShadeController(shade_id, shade_config, config['mqtt'], config['rf'])

    # controllers = {f"{shade_config['']}": ShadeController(shade_id, shade_config, config['mqtt'], config['rf']) for shade_id, shade_config in config['shades'].items()}
    for shade_id, controller in controllers.items():
        controller.start()

    pi = pigpio.pi()
    RX_port = 27
    print(controllers.keys())
    rxc = RX(pi, gpio=RX_port, callback=decode, controllers=controllers)

    for shade_id, controller in controllers.items():
        controller.join()

    rxc.cancel()  # Cancel the receiver.

    pi.stop()

    logging.info("Exiting")
