import logging
import time
from tx import TX
import yaml
import paho.mqtt.client as mqtt_client
import queue

logging.basicConfig(level=logging.INFO)


class ShadesDriver:
    def __init__(self, GPIO_port, config_file):
        self.logger = logging.getLogger(self.__class__.__name__)
        with open(config_file, 'r') as fp:
            self.config = yaml.load(fp, Loader=yaml.FullLoader)
        self.GPIO_port = GPIO_port
        self.actions = self.config['rf']['actions']
        self.tx_radio = TX(gpio=self.GPIO_port, repeats=3, bits=38, preamble=(4800, 1400), gap=4200, t0=360, t1=720)
        self.remotes = self.config['rf']['remotes']
        self.factory_prefix = self.config['rf']['factory_prefix']
        self.connected = False
        self.client = None
        self.action_queue = queue.Queue()
        self.connect()
        self.main()

    def main(self):
        while True:
            shade_id, action = self.action_queue.get()
            self.logger.info(f"{shade_id} {action}")
            if shade_id not in self.config['shades']:
                self.logger.warning(f"Unknown shade id: {shade_id}")
                continue
            shade_config = self.config['shades'][shade_id]
            self.logger.info(f"{shade_config}")
            remote_code = self.remotes[shade_config['remote']]
            channel = int(shade_config['channel'])
            if action == "open":
                self.open(remote_code, channel)
            elif action == "close":
                self.close(remote_code, channel)
            elif action == "stop":
                self.stop(remote_code, channel)
            else:
                self.logger.warning(f"Unknown action: {action}")




    def connect(self):
        self.client = mqtt_client.Client(f"shades_transmitter")
        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_message = self.on_message
        self.client.on_subscribe = self.on_subscribe
        self.client.connect(self.config['mqtt']['host'])  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        self.client.subscribe(f"{self.config['mqtt']['topic_prefix']}/+/set")
        #self.logger.info(f"Subscribed to: {self.config['mqtt']['command_topic_prefix']}")

    def on_message(self, client, userdata, message):
        action = message.payload.decode('utf-8').lower()
        topic = message.topic
        shade_id = topic.split('/')[-2]
        self.action_queue.put((shade_id, action))
        #self.logger.info(f"{action} {topic} {shade_id}")

    def on_subscribe(self, client, userdata, mid, granted_qos):
        self.logger.info(f"Subscribed to topic")

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")

    def get_code(self, remote_id, channel, action):
        return (self.factory_prefix << 21) | (int(remote_id) << 10) | (int(channel) << 7) | self.actions[action]

    def open(self, remoteid, channel):
        self.tx_radio.send(self.get_code(remoteid, channel, "OPEN"))
        self.tx_radio.send(self.get_code(remoteid, channel, "OPEN"))
        self.tx_radio.send(self.get_code(remoteid, channel, "OPEN"))
        self.tx_radio.send(self.get_code(remoteid, channel, "UP_STOP"))
        self.tx_radio.send(self.get_code(remoteid, channel, "UP_STOP"))

    def stop(self, remoteid, channel):
        self.tx_radio.send(self.get_code(remoteid, channel, "STOP"))
        self.tx_radio.send(self.get_code(remoteid, channel, "STOP"))

    def close(self, remoteid, channel):
        self.tx_radio.send(self.get_code(remoteid, channel, "CLOSE"))
        self.tx_radio.send(self.get_code(remoteid, channel, "CLOSE"))
        self.tx_radio.send(self.get_code(remoteid, channel, "CLOSE"))
        self.tx_radio.send(self.get_code(remoteid, channel, "DOWN_STOP"))
        self.tx_radio.send(self.get_code(remoteid, channel, "DOWN_STOP"))


if __name__ == '__main__':
    tx_port = 17
    transmitter = ShadesDriver(tx_port, '/config/config.yaml')

#670 1 - obyvak male okno