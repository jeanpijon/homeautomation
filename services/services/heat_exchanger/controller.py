import time
from multiprocessing import Process
import logging

import serial.rs485
import yaml


class HeatExchanger(Process):
    def __init__(self, config_path, q, event):
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.config = None
        self.controller = None
        self.initialize(config_path)
        self.fan_speed = 0
        self.mode = f'idle'
        self.stop = False
        self.q = q
        self.event = event

    def load_config(self, path):
        with open(path) as c:
            self.config = yaml.load(c, Loader=yaml.FullLoader)
        for mode, command in self.config['modes'].items():
            self.config['modes'][mode] = [ord(c) for c in command]

    def initialize(self, config_path):
        self.logger.info("Initializing")
        self.load_config(config_path)
        self.controller = serial.rs485.RS485(self.config['serial_device'], parity=serial.PARITY_NONE,
                                             stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS)
        self.logger.info("Initialized")

    def run(self):
        self.logger.info(f"Starting run")
        self.controller.flushInput()
        self.controller.write(self.config['modes']['idle_0'])
        time.sleep(1)
        self.controller.write(self.config['modes']['idle_0'])
        time.sleep(1)
        self.logger.info(f"Starting control loop")
        while not self.event.is_set():
            mode = f"{self.mode}_{self.fan_speed}"
            try:
                requested_mode = self.config['modes'][mode]
            except KeyError:
                requested_mode = self.config['modes']['idle_0']
            self.controller.write(requested_mode)
            response = self.controller.read(74)
            str_data_response = response[19:-1].decode('utf-8')
            data = [int(str_data_response[i] + str_data_response[i + 1], 16) for i in
                    range(0, len(str_data_response), 2)]
            self.logger.debug(f"mode: {self.mode} fan: {self.fan_speed}")
            self.logger.debug(f"fan out->in: {data[1]}; fan in->out:{data[2]}")
            self.logger.debug(
                f"out ingress: {data[19]}; out egress: {data[20]}; room egress: {data[21]}; room ingress: {data[22]}")
            payload = {"state": {"mode": self.mode,
                          "fan": self.fan_speed
                          }}
            self.q.put(payload)
            payload = {"sensors": {"temperature.out_ingress": data[19],
                            "temperature.out_egress": data[20],
                            "temperature.in_ingress": data[22],
                            "temperature.in_egress": data[21],
                            "power_factor.fan_ingress": data[1],
                            "power_factor.fan_egress": data[2]
                            }}

            self.q.put(payload)
            self.event.wait(timeout=1)
        self.logger.info("Terminating")
        self.controller.write(self.config['modes']['idle_0'])
        time.sleep(1)
        self.controller.write(self.config['modes']['idle_0'])
        time.sleep(1)
        self.logger.info("Terminated")
