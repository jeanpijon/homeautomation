import json
import queue
from multiprocessing import Event

from heat_exchanger.controller import HeatExchanger
from common.mqtt import BaseMQTTAdapter
from common.mqtt import MQTTSensor


class HeatExchangerMQTTAdapter(BaseMQTTAdapter):
    def __init__(self, config_path):
        super().__init__(config_path)
        self.config_path = config_path
        self.run_allowed = True
        self.q = queue.Queue()
        self.event = Event()
        self.device = {
                "manufacturer": "MultiVac Pardubice",
                "suggested_area": "technicka-mistnost",
                "identifiers": "venus-heat-exchanger-1"
            }
        self.state = {"mode": None, "fan": None}
        self.sensors = {}
        self.initialize()

    def initialize(self):
        self.topic_prefix = f"homeassistant/climate/{self.config['id']}"
        self.configuration_topic = f"{self.topic_prefix}/config"
        self.heat_exchanger = HeatExchanger(self.config_path, self.q, self.event)
        self.client.message_callback_add(f"{self.topic_prefix}/mode/set", self.mode_callback)
        self.client.message_callback_add(f"{self.topic_prefix}/fan/set", self.fan_callback)
        self.connect()

    def run(self):
        self.logger.info(f"Starting")
        self.heat_exchanger.start()
        self.client.subscribe(f"{self.topic_prefix}/#")
        config = {
            "~": self.topic_prefix,
            "name": self.config['name'],
            "mode_cmd_t": "~/mode/set",
            "mode_stat_t": "~/mode/state",
            "fan_mode_command_topic": "~/fan/set",
            "fan_mode_state_topic": "~/fan/state",
            "json_attributes_topic": "~/attributes",
            "fan_modes": ["1","2","3"],
            "min_temp": "15",
            "max_temp": "25",
            "temp_step": "0.5",
            "modes": ["off", "auto", "cool"],
            "unique_id": "venus-heat-exchanger-1",
            "device":self.device
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)
        while self.run_allowed:
            payload = self.q.get()
            if "state" in payload.keys():
                if payload["state"]['mode'] == "idle":
                    mode = "off"
                else:
                    mode = payload["state"]['mode']

                if mode != self.state['mode']:
                    self.client.publish(f"{self.topic_prefix}/mode/state", payload=mode, qos=0, retain=True)
                    self.state['mode'] = mode
                if payload['state']['fan'] != self.state['fan']:
                    self.client.publish(f"{self.topic_prefix}/fan/state",
                                    payload=f"{payload['state']['fan']}", qos=0, retain=True)
                    self.state['fan'] = payload['state']['fan']
            elif "sensors" in payload.keys():
                for sensor_name_with_class, sensor_value in payload["sensors"].items():
                    device_class, sensor_name = sensor_name_with_class.split(".")
                    if device_class == "none":
                        device_class = None
                    if sensor_name not in self.sensors.keys():
                        new_sensor = MQTTSensor(f"{self.config['machine_name']}_{sensor_name}", self.client, self.device, device_class, f"{self.config['machine_name']}_{sensor_name}")
                        self.sensors[sensor_name] = new_sensor
                    self.sensors[sensor_name](sensor_value)

        print("Requesting stop on HE")
        self.heat_exchanger.stop = True
        self.heat_exchanger.join(60)

    def stop(self):
        self.logger.info(f"Stop requested")
        self.run_allowed = False

    def mode_callback(self, client, userdata, message):
        self.logger.info(f"Switching to mode {message.payload}")
        mode = message.payload.decode("utf-8")
        if mode == "off":
            mode = "idle"
            self.heat_exchanger.fan_speed = 0
        else:
            if self.heat_exchanger.fan_speed == 0:
                self.heat_exchanger.fan_speed = 1
        self.heat_exchanger.mode = mode

    def fan_callback(self, client, userdata, message):
        self.logger.info(f"Switching to fan speed {message.payload}")
        self.heat_exchanger.fan_speed = int(message.payload.decode("utf-8"))
