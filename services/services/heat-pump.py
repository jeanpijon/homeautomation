import logging
import argparse

from common.utils import GracefulKiller
from heat_pump.mqtt_bridge import HeatPumpMQTTAdapter



def main(args):
    logging.basicConfig(level=args.verbose)
    heat_pump = HeatPumpMQTTAdapter(args.config)
    GracefulKiller(heat_pump)
    heat_pump.run()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MQTT bridge for controlling Spirala Heat Pump')
    parser.add_argument('--config', action='store', type=str, default="/opt/homeassistant-dev/services/config/heat_pump/config.yaml")
    parser.add_argument('--verbose', action='store', type=int, default=0)

    args, unknown = parser.parse_known_args()
    main(args)