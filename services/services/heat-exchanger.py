import signal


import logging
import argparse

from common.utils import GracefulKiller

from heat_exchanger.mqtt_bridge import HeatExchangerMQTTAdapter


def main(args):
    logging.basicConfig(level=args.verbose)
    he = HeatExchangerMQTTAdapter(args.config)
    GracefulKiller(he)
    he.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MQTT bridge for controlling Venus Heat Exchanger')
    parser.add_argument('--config', action='store', type=str, default="/opt/homeassistant-dev/services/config/heat_exchanger/config.yaml")
    parser.add_argument('--verbose', action='store', type=int, default=0)

    args, unknown = parser.parse_known_args()
    main(args)