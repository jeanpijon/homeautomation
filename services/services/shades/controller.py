import logging
from enum import Enum
from multiprocessing import Process

import paho.mqtt.publish as publish
import queue
import threading
import time
import pigpio
from shades.tx import TX


class FsmState(Enum):
    HANDSHAKE1 = 1
    HANDSHAKE2 = 2
    DATA = 3
    ONE = 4
    ZER0 = 5
    FINAL = 6


class ShadeRFController(Process):
    """
    A class to read the wireless codes transmitted by radio443 MHz
    wireless fobs.
    """

    def __init__(self, config, rx_q, tx_q, glitch=150):
        """
        Instantiate with the Pi and the GPIO connected to the wireless
        receiver.

        If specified the callback will be called whenever a new code
        is received.  The callback will be passed the code, the number
        of bits, the length (in us) of the gap, short pulse, and long
        pulse.

        Codes with bit lengths outside the range min_bits to max_bits
        will be ignored.

        A glitch filter will be used to remove edges shorter than
        glitch us long from the wireless stream.  This is intended
        to remove the bulk of radio noise.
        """
        super().__init__()
        self.logging = logging.getLogger(self.__class__.__name__)
        self.pi = pigpio.pi()
        self.tx = TX(self.pi, gpio=config['rf']['rx_gpio'], repeats=3, bits=38, preamble=(4800, 1400), gap=4200, t0=360, t1=720)
        self.rx_q = rx_q
        self.tx_q = tx_q
        self.config = config
        self.gpio = config['rf']['rx_gpio']
        self.actions = {v: k for k, v in config['rf']['actions'].items()}
        self.tolerance = 10
        self.glitch = glitch
        self._state = FsmState.HANDSHAKE1
        self._remote_bit = 0
        self._remote_code = 0
        self._check_bit = None
        self._timings = []
        self._t = 0

        self.pi.set_mode(self.gpio, pigpio.INPUT)
        self.pi.set_pull_up_down(self.gpio, pigpio.PUD_OFF)
        self.pi.set_glitch_filter(self.gpio, glitch)

        self._last_edge_tick = self.pi.get_current_tick()
        self._cb = self.pi.callback(self.gpio, pigpio.EITHER_EDGE, self._cbf)
        self._counter = 0

    def run(self):
        self.logging.info("Starting event loop")
        while True:
            self.logging.info("Waiting for the codes")
            code = self.tx_q.get()
            self.logging.info(f"Code obtained {code}")
            self.tx.send(code)


    def decode(self, code, check, counter):
        bin_code = bin(code)
        if len(bin_code) == 40:
            if bin_code[-3] == str(check):
                channel_id = int(bin_code[30:33], 2)
                remote_id = int(bin_code[19:30], 2)
                action = self.actions[int(bin_code[-7:], 2)]
                if action in ["CLOSE", "STOP", "OPEN"]:
                    self.rx_q.push((remote_id, channel_id, action))
            else:
                self.logging.error(f"Wrong checksum {bin_code[-3]} != {check}")

    def cmp_tolerance(self, a, b):
        factor = float(b) / 100 * self.tolerance
        ret = (b - factor <= a <= b + factor)
        return ret

    def _cbf(self, g, l, t):
        # print("A")
        # pi.write(LED_PORT, 1)
        duration = pigpio.tickDiff(self._last_edge_tick, t)
        self._timings.append(duration)
        # print(edge_len)
        self._last_edge_tick = t
        if self._state == FsmState.HANDSHAKE1:
            self._timings = [duration,]
            self._t = time.time_ns()
            #self.pi.write(LED_PORT, 0)
            self._remote_bit = 0
            self._remote_code = 0
            if self.cmp_tolerance(duration, 4800):
                self._state = FsmState.HANDSHAKE2
        elif self._state == FsmState.HANDSHAKE2:
            if self.cmp_tolerance(duration, 1440):
                self._state = FsmState.DATA
        elif self._state == FsmState.DATA:
            if self.cmp_tolerance(duration, 360):
                self._state = FsmState.ZER0
            elif self.cmp_tolerance(duration, 720):
                self._state = FsmState.ONE
            else:
                self._state = FsmState.HANDSHAKE1
        elif self._state == FsmState.ONE:
            if self.cmp_tolerance(duration, 360):
                self._remote_code <<= 1
                self._remote_code |= 1
                self._remote_bit += 1

                self._state = FsmState.DATA
            elif duration > 3900:
                self._check_bit = 0
                self._state = FsmState.FINAL
                #print(f"ONE final duration {duration}")
            else:
                #print(f"Wrong One {duration}")
                self._state = FsmState.HANDSHAKE1

        elif self._state == FsmState.ZER0:
            if self.cmp_tolerance(duration, 720):
                self._remote_code <<= 1
                self._remote_bit += 1
                self._state = FsmState.DATA
            elif duration > 3900:
                #print(f"ZERO final duration {duration}")
                self._check_bit = 1
                self._state = FsmState.FINAL
            else:
                #print(f"Wrong Zero {duration}")
                self._state = FsmState.HANDSHAKE1
        elif self._state == FsmState.FINAL:

            #self.decode(self._remote_code, self._check_bit, self._counter)

            bin_code = bin(self._remote_code)
            if len(bin_code) == 40:
                if bin_code[-3] == str(self._check_bit):
                    self.rx_q.push(bin_code)

            #print(self._timings)
            self._timings = []
            #pi.write(LED_PORT, 1)
            self._counter += 1
            self._state = FsmState.HANDSHAKE1

    def cancel(self):
        """
        Cancels the wireless code receiver.
        """
        if self._cb is not None:
            self.pi.set_glitch_filter(self.gpio, 0)  # Remove glitch filter.
            self._cb.cancel()
            self._cb = None
            self.pi.stop()

#
# LED_PORT = 26
# RX_port = 27
#
# if __name__ == "__main__":
#     actions = {
#         0b0001000: "OPEN",  # up_start
#         0b0001111: "STOP", #up_stop
#         0b0101010: "STOP",  # stop
#         0b0011001: "CLOSE",  # down_start
#         0b0011110: "STOP", # "down_stop" : 0b0011110, #down_stop
#         0b1100110: "PAIR"   # pair
#     }
#
#     def decode(code, check, counter):
#         #print(threading.get_ident())
#         #print(time.time())
#         bin_code = bin(code)
#         print(code, bin_code)
#         print(check)
#         if len(bin_code) == 40:
#             if bin_code[-3] == str(check):
#                 channel_id = int(bin_code[30:33], 2)
#                 remote_id = int(bin_code[19:30], 2)
#                 action = actions[int(bin_code[-7:], 2)]
#
#                 print(f"{time.time()} Channel ID: {channel_id} RemoteID: {remote_id} Action: {action}")
#                 publish.single("home-assistant/mqtt-covers/set-rf", f"{action}_{remote_id}_{channel_id}", hostname="localhost")
#             else:
#                 print(f"Wrong checksum {bin_code[-3]} != {check}")
#         # else:
#         #     print(f"Wrong length {len(bin_code)} != 40")
#
#
#     pi = pigpio.pi()  # Connect to local Pi.
#
#     pi.set_mode(LED_PORT, pigpio.OUTPUT)
#     pi.write(LED_PORT, 0)
#
#     rxc = RX(pi, gpio=RX, callback=decode)
#
#     time.sleep(10000000)
#
#
#     rxc.cancel()  # Cancel the receiver.
#
#     pi.stop()  # Disconnect from local Pi.

