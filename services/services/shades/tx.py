import logging
import time
import pigpio

class TX():
    """
    A class to transmit the wireless codes sent by radio443 MHz
    wireless fobs.
    """

    def __init__(self, pi, gpio, repeats=6, bits=24, preamble=(4800,1440), gap=9000, t0=300, t1=900):
        """
        Instantiate with the Pi and the GPIO connected to the wireless
        shades_driver.

        The number of repeats (default 6) and bits (default 24) may
        be set.

        The pre-/post-amble gap (default 9000 us), short pulse length
        (default 300 us), and long pulse length (default 900 us) may
        be set.
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.pi = pi
        self.gpio = gpio
        self.repeats = repeats
        self.bits = bits
        self.preamble = preamble
        self.gap = gap
        self.t0 = t0
        self.t1 = t1

        self._make_waves()

        self.pi.set_mode(gpio, pigpio.OUTPUT)

    def _make_waves(self):
        """
        Generates the basic waveforms needed to transmit codes.
        """
        wf = []
        wf.append(pigpio.pulse(1 << self.gpio, 0, self.preamble[0]))
        wf.append(pigpio.pulse(0, 1 << self.gpio, self.preamble[1]))
        self.pi.wave_add_generic(wf)
        self._preamble = self.pi.wave_create()

        wf = []
        wf.append(pigpio.pulse(1 << self.gpio, 0, self.t0))
        wf.append(pigpio.pulse(0, 1 << self.gpio, self.t1))
        self.pi.wave_add_generic(wf)
        self._wid0 = self.pi.wave_create()

        wf = []
        wf.append(pigpio.pulse(1 << self.gpio, 0, self.t1))
        wf.append(pigpio.pulse(0, 1 << self.gpio, self.t0))
        self.pi.wave_add_generic(wf)
        self._wid1 = self.pi.wave_create()

        wf = []
        wf.append(pigpio.pulse(1 << self.gpio, 0, self.t0))
        wf.append(pigpio.pulse(0, 1 << self.gpio, 4070))
        self.pi.wave_add_generic(wf)
        self._check0 = self.pi.wave_create()

        wf = []
        wf.append(pigpio.pulse(1 << self.gpio, 0, self.t1))
        wf.append(pigpio.pulse(0, 1 << self.gpio, 4300))
        self.pi.wave_add_generic(wf)
        self._check1 = self.pi.wave_create()

    def set_repeats(self, repeats):
        """
        Set the number of code repeats.
        """
        if 1 < repeats < 100:
            self.repeats = repeats

    def set_bits(self, bits):
        """
        Set the number of code bits.
        """
        if 5 < bits < 65:
            self.bits = bits

    def set_timings(self, gap, t0, t1):
        """
        Sets the code gap, short pulse, and long pulse length in us.
        """
        self.gap = gap
        self.t0 = t0
        self.t1 = t1

        self.pi.wave_delete(self._preamble)
        self.pi.wave_delete(self._check0)
        self.pi.wave_delete(self._check1)
        self.pi.wave_delete(self._wid0)
        self.pi.wave_delete(self._wid1)

        self._make_waves()

    def send(self, code):
        """
        Transmits the code (using the current settings of repeats,
        bits, gap, short, and long pulse length).
        """
        self.logger.info(f"Sending {code}")
        chain = [self._preamble, 255, 0]
        checksum = None
        chain += [self._wid0] #prefix with one zero
        bit = (1 << (self.bits - 1))
        for i in range(self.bits):
            if code & bit:
                print(1, end='')
                chain += [self._wid1]
            else:
                print(0, end='')
                chain += [self._wid0]
            bit = bit >> 1

            if i == 35:
                #print(code & bit)
                if code & bit:
                    checksum = 1
                else:
                    checksum = 0
        if checksum == 0:
            chain += [self._check1, 255, 1, self.repeats, 0]
        elif checksum == 1:
            chain += [self._check0, 255, 1, self.repeats, 0]
        print("")
        self.pi.wave_chain(chain)

        while self.pi.wave_tx_busy():
            time.sleep(0.1)

    def cancel(self):
        """
        Cancels the wireless code shades_driver.
        """
        self.pi.wave_delete(self._preamble)
        self.pi.wave_delete(self._check0)
        self.pi.wave_delete(self._check1)
        self.pi.wave_delete(self._wid0)
        self.pi.wave_delete(self._wid1)



