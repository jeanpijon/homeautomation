import json
import logging
import time
from copy import deepcopy
import queue

import paho.mqtt.client as mqtt_client
import yaml

from heat_exchanger.controller import HeatExchanger
from common.mqtt import BaseMQTTAdapter
from common.mqtt import MQTTSensor



class FSMStates(Enum):
    IDLE = 1
    OPENING = 2
    CLOSING = 3


class ShadeController(BaseMQTTAdapter):
    def __init__(self, shade_id, shade_config, mqtt_config, rf_config):
        super().__init__()
        self.shade_id = shade_id
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.shade_id}")
        self.logger.info(f"Initializing")
        self.logger.info(shade_config)
        self.shade_config = shade_config
        self.connected = False
        self.mqtt_config = mqtt_config
        self.rf_config = rf_config
        self.client = None
        self.position_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/position"
        self.command_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/set"
        self.timeout = 1
        self.queue = queue.Queue()
        self.fsm_state = FSMStates.IDLE

    def connect(self):
        self.client = mqtt_client.Client(f"shade_{self.shade_id}")
        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_message = self.on_message
        self.client.connect(self.mqtt_config['host'])  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        return True

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")

    def run(self):
        self.connect()
        self.register()
        self.logger.info(f"Starting: {self.shade_id}")
        timeout = 0
        while True:
            try:
                command = self.queue.get(timeout=timeout)
            except queue.Empty:
                command = "NOP"

            if self.fsm_state == FSMStates.IDLE:
                if command == "STOP":
                    # self.client.publish(self.position_topic, 50)
                    self.logger.info(f"Stop in IDLE, nothing to be done")
                elif command == "OPEN":
                    self.logger.info(f"Set OPEN")
                    if self.position < self.shade_config['dark']:
                        pass
                    else:
                        pass
                    self.client.publish(self.position_topic, 100)
                elif command == "CLOSE":
                    self.logger.info(f"Set CLOSE")
                    self.client.publish(self.position_topic, 0)
                elif command == "NOP":
                    pass

            elif self.fsm_state == FSMStates.OPENING:
                pass
            elif self.fsm_state == FSMStates.CLOSING:
                pass

    def on_message(self, client, userdata, message):
        # print("%s: %s %s" % (self.shade_id, message.topic, message.payload))
        action = message.payload.decode('utf-8')
        topic = message.topic
        shade_id = topic.split('/')[-2]

    def register(self):
        topic = f"homeassistant/cover/shade{self.shade_id}/config"
        payload = {
            "name": f"{self.shade_config['name']}",
            "command_topic": self.command_topic,
            "position_topic": self.position_topic,
            "position_open": self.shade_config['dark'],
            "position_closed": 0,
            "retain": True,
            "optimistic": True,
            "unique_id": f"{self.shade_id}.cover",
            "device": {
                "identifiers": f"rc.{self.shade_id}"
            },
            "device_class": "shade"
        }
        self.logger.info(f"Registering: {self.shade_id}")
        self.client.publish(topic, json.dumps(payload))


actions = {
    0b0001000: "OPEN",  # up_start
    0b0001111: "UP_STOP",  # up_stop
    0b0101010: "STOP",  # stop
    0b0011001: "CLOSE",  # down_start
    0b0011110: "DOWN_STOP",  # "down_stop" : 0b0011110, #down_stop
    0b1100110: "PAIR"  # pair
}



class ShadesMQTTAdapter(BaseMQTTAdapter):
    def __init__(self, config_path):
        super().__init__(config_path)
        self.config_path = config_path
        self.run_allowed = True
        self.q = queue.Queue()
        self.device = {
                "manufacturer": "Unnamed China",
                "suggested_area": "technicka-mistnost",
                "identifiers": "venus-heat-exchanger-1"
            }
        self.state = {"mode": None, "fan": None}
        self.sensors = {}
        self.initialize()

    def initialize(self):
        self.topic_prefix = f"homeassistant/climate/{self.config['id']}"
        self.configuration_topic = f"{self.topic_prefix}/config"
        self.heat_exchanger = HeatExchanger(self.config_path, self.q)
        self.client.message_callback_add(f"{self.topic_prefix}/mode/set", self.mode_callback)
        self.client.message_callback_add(f"{self.topic_prefix}/fan/set", self.fan_callback)
        self.connect()

    def run(self):
        self.logger.info(f"Starting")
        self.heat_exchanger.start()
        self.client.subscribe(f"{self.topic_prefix}/#")
        config = {
            "~": self.topic_prefix,
            "name": self.config['name'],
            "mode_cmd_t": "~/mode/set",
            "mode_stat_t": "~/mode/state",
            "fan_mode_command_topic": "~/fan/set",
            "fan_mode_state_topic": "~/fan/state",
            "json_attributes_topic": "~/attributes",
            "fan_modes": ["1","2","3"],
            "min_temp": "15",
            "max_temp": "25",
            "temp_step": "0.5",
            "modes": ["off", "auto", "cool"],
            "unique_id": "venus-heat-exchanger-1",
            "device":self.device
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)
        while self.run_allowed:
            payload = self.q.get()
            if "state" in payload.keys():
                if payload["state"]['mode'] == "idle":
                    mode = "off"
                else:
                    mode = payload["state"]['mode']

                if mode != self.state['mode']:
                    self.client.publish(f"{self.topic_prefix}/mode/state", payload=mode, qos=0, retain=True)
                    self.state['mode'] = mode
                if payload['state']['fan'] != self.state['fan']:
                    self.client.publish(f"{self.topic_prefix}/fan/state",
                                    payload=f"{payload['state']['fan']}", qos=0, retain=True)
                    self.state['fan'] = payload['state']['fan']
            elif "sensors" in payload.keys():
                for sensor_name_with_class, sensor_value in payload["sensors"].items():
                    device_class, sensor_name = sensor_name_with_class.split(".")
                    if device_class == "none":
                        device_class = None
                    if sensor_name not in self.sensors.keys():
                        new_sensor = MQTTSensor(f"{self.config['machine_name']}_{sensor_name}", self.client, self.device, device_class, f"{self.config['machine_name']}_{sensor_name}")
                        self.sensors[sensor_name] = new_sensor
                    self.sensors[sensor_name](sensor_value)

        print("Requesting stop on HE")
        self.heat_exchanger.stop = True
        self.heat_exchanger.join(60)

    def stop(self):
        self.logger.info(f"Stop requested")
        self.run_allowed = False

    def mode_callback(self, client, userdata, message):
        self.logger.info(f"Switching to mode {message.payload}")
        mode = message.payload.decode("utf-8")
        if mode == "off":
            mode = "idle"
            self.heat_exchanger.fan_speed = 0
        else:
            if self.heat_exchanger.fan_speed == 0:
                self.heat_exchanger.fan_speed = 1
        self.heat_exchanger.mode = mode

    def fan_callback(self, client, userdata, message):
        self.logger.info(f"Switching to fan speed {message.payload}")
        self.heat_exchanger.fan_speed = int(message.payload.decode("utf-8"))
