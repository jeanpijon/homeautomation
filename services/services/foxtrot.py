import signal


import logging
import argparse

from foxtrot.mqtt_bridge import FoxtrotMQTTAdapter


def main(args):
    logging.basicConfig(level=args.verbose)
    device = FoxtrotMQTTAdapter(args.config)
    device.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MQTT bridge for TECO Foxtrot')
    parser.add_argument('--config', action='store', type=str, default="/opt/homeassistant-dev/services/config/foxtrot/config.yaml")
    parser.add_argument('--verbose', action='store', type=int, default=0)

    args, unknown = parser.parse_known_args()
    main(args)