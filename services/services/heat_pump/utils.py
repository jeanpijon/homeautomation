def unparse(value, value_type, enums):
    if value_type == "temperature":
        return int(float(value) * 256)
    elif value_type == "temperature_times2":
        return int(float(value) * 128)
    elif value_type == "percent":
        return int(float(value) * 128)
    elif value_type.startswith("enum_"):
        for key, val in enums[value_type].items():
            if val == value:
                return key
    elif value_type == "counter":
        return int(value)
    elif value_type == "bool":
        return int(value)
    else:
        print(f"Unparsing unknown datatype {value_type}")
        return value

def parse(value, value_type, enums):
    if value_type == "temperature":
        return float(f"{(value / 256):.1f}"), "°C"
    elif value_type == "temperature_times2":
        return float(f"{(value / 128):.1f}"), "°C"
    elif value_type == "percent":
        return float(f"{(value / 128):.1f}"), "%"
    elif value_type.startswith("enum_"):
        return enums[value_type][value], ""
    elif value_type == "counter":
        return value, ""
    elif value_type == "bool":
        return value, ""
    else:
        return value, ""
