import json
from multiprocessing import Queue, Event

from heat_pump.controller import HeatPump
from common.mqtt import BaseMQTTAdapter, MQTTNumber, MQTTSwitch, MQTTSelect
from common.mqtt import MQTTSensor


class HeatPumpMQTTAdapter(BaseMQTTAdapter):
    def __init__(self, config_path):
        super().__init__(config_path)
        self.config_path = config_path
        self.run_allowed = True
        self.q = Queue()
        self.control_q = Queue()
        self.event = Event()
        self.devices = {}
        self.master_device = None
        self.initialize()

    def get_mqtt_class(self, mqtt_class):
        if mqtt_class == "sensor":
            return MQTTSensor
        elif mqtt_class == "number":
            return MQTTNumber
        elif mqtt_class == "switch":
            return MQTTSwitch
        elif mqtt_class == "select":
            return MQTTSelect
        else:
            self.logger.warning(f"Unhandled mqtt class: {mqtt_class}")
            return False

    def initialize(self):
        self.master_device = {
            "manufacturer": "Spirala",
            "suggested_area": "technicka-mistnost",
            "identifiers": self.config['id'],
            "name": "spirala"
        }
        self.topic_prefix = f"homeassistant/climate/{self.config['id']}"
        self.configuration_topic = f"{self.topic_prefix}/config"
        self.heat_pump = HeatPump(self.config_path, self.q, self.control_q, self.event)
        for device_name, device in self.heat_pump.devices.items():
            mqtt_device_class = self.get_mqtt_class(device['mqtt_class'])
            if mqtt_device_class:
                self.devices[device_name] = mqtt_device_class(device_name, device, self.client, self.master_device)
                if self.devices[device_name].set_topic:
                    self.logger.info(f"Setting callback for {device_name} on {self.devices[device_name].set_topic}")
                    self.client.message_callback_add(f"{self.devices[device_name].set_topic}", self.set_callback)
        self.connect()
        for device_name, device in self.devices.items():
            device.register()
            if self.devices[device_name].set_topic:
                self.client.subscribe(f"{self.devices[device_name].set_topic}", 0)

    def run(self):
        self.logger.info(f"Starting")
        self.heat_pump.start()
        self.client.subscribe(f"{self.topic_prefix}/#")
        config = {
            "~": self.topic_prefix,
            "name": self.config['name'],
            "mode_stat_t": "~/mode/state",
            "modes": ["auto"],
            "unique_id": self.master_device['identifiers'],
            "device": self.master_device
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)
        while self.run_allowed:
            device_name, value = self.q.get()
            # self.logger.info(f"Got PAYLOAD {device_name}, {value}")
            if device_name in self.devices:
                self.devices[device_name].set((value))

        self.logger.info("Requesting stop on Heat Pump")
        self.event.set()
        self.heat_pump.join()
        self.logger.info("Exiting")

    def stop(self):
        self.logger.info(f"Stop requested")
        self.run_allowed = False

    def set_callback(self, client, userdata, message):
        payload = message.payload.decode("utf-8")
        device_id = message.topic.split('/')[-2]
        self.logger.info(f"Obtained data {payload}, {device_id}")
        self.control_q.put((device_id, payload))

#
# hpma = HeatPumpMQTTAdapter("/opt/homeassistant-dev/services/config/heat_pump/config.yaml")
# hpma.run()
