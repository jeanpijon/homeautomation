import time
from multiprocessing import Process, Event, Queue
import logging
from queue import Empty

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import yaml
from heat_pump.utils import parse, unparse

logging.basicConfig(level=logging.INFO)
#logging.getLogger("pymodbus.client.sync").setLevel(logging.DEBUG)
logging.getLogger("pymodbus").setLevel(logging.ERROR)
class HeatPump(Process):
    def __init__(self, config_path, state_q: Queue, set_q: Queue, event):
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.config = None
        self.controller = None
        self.invalid_value = 51968
        self.baseaddress = 0
        self.unit_address = 60
        self.state_q = state_q
        self.set_q = set_q
        self.devices = {}
        self.initialize(config_path)
        self._stop_event = event

    def load_config(self, path):
        with open(path) as c:
            self.config = yaml.load(c, Loader=yaml.FullLoader)

    def initialize(self, config_path):
        self.logger.info("Initializing")
        self.load_config(config_path)
        self.controller = ModbusClient(method='rtu', timeout=5., port='/dev/ttyUSB1',baudrate=19200,stopbits=1,parity='E', strict=False)

        for device_name, device in self.config['devices'].items():
            #self.logger.info(device_address)
            #if "alt" in device.keys():
            if device["mqtt_class"] == "select":
                device["select_values"] = list(self.config["enums"][device["type"]].values())
            self.devices[device_name] = device
        #self.logger.info(self.devices)
        self.logger.info("Initialized")

    def set_register(self, device_name, value):
        value_type = self.devices[device_name]["type"]
        device_address = int(self.devices[device_name]["address"])
        unparsed_value = unparse(value, value_type, self.config['enums'])
        print((device_address, unparsed_value, type(unparsed_value)))
        while True:
            response = self.controller.write_register(unit=self.unit_address, address=device_address, value=unparsed_value)
            if response.isError():
                self.logger.error(f"Error setting {device_name} to {value}, retrying")
            else:
                value, unit = parse(response.value, value_type, self.config['enums'])
                self.logger.info(f"Set {device_address} to {value}{unit} successful")
                return

    def get_register(self, device_name, value_type):
        device_address = self.devices[device_name]["address"]
        response = self.controller.read_holding_registers(unit=self.unit_address, address=self.baseaddress + device_address, count=1, )
        if response.isError():
            #self.logger.error(f"Unread register {ii}")
            return False, False
        value, unit = parse(response.registers[0], value_type, self.config['enums'])
        return value, unit


    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        self.controller.set_debug(logging.ERROR)
        self.logger.info(f"Starting run")
        if not self.controller.connect():
            raise RuntimeError("Cannot connect")
        while not self.stopped():
            if not self.set_q.empty():
                try:
                    while True:
                        payload = self.set_q.get_nowait()
                        device_name, value = payload
                        self.logger.info(f"Setting {device_name} to {value}")
                        self.set_register(device_name, value)
                except Empty:
                    pass

            for device_name, device in self.devices.items():
                value, unit = self.get_register(device_name, device["type"])
                if value == self.invalid_value or (value, unit) == (False, False):
                    #self.logger.warning(f"Device {device_name} returned invalid value")
                    continue
                self.state_q.put((device_name, value))
                #self.logger.info(f"{device_name}, {value}")
            self._stop_event.wait(timeout=self.config['poll_interval'])
        return
# #
# ev = Event()
# q1 = Queue()
# q2 = Queue()
# q2.put(("temperature_water_requested", 48))
# hp = HeatPump("/opt/homeassistant-dev/services/config/heat_pump/config.yaml", q1, q2, ev)
# hp.start()

