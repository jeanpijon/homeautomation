import signal


class GracefulKiller:
    def __init__(self, object_to_kill):
        self.object_to_kill = object_to_kill
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        self.object_to_kill.stop()