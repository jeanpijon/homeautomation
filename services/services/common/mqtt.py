import json
import logging
import time

import yaml
import paho.mqtt.client as mqtt_client

class BaseMQTTAdapter():
    def __init__(self, config_path):
        super().__init__()
        with open(config_path) as c:
            self.config = yaml.load(c, Loader=yaml.FullLoader)
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.config['id']}")
        self.connected = False
        self.mqtt_config = self.config['mqtt']
        self.client = None
        self.timeout = 1
        self.device_id = self.config['id']
        self.client = mqtt_client.Client(f"{self.device_id}-{time.time()}")

    def connect(self):

        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_disconnect = self.on_disconnect
        self.client.connect(self.mqtt_config['host'])  # connect to broker
        self.client.loop_start()  # start the loop
        self.logger.info("Waiting for mqtt connection")
        while not self.connected:  # Wait for connection
            time.sleep(0.1)
        return True

    def on_disconnect(self):
        raise NotImplementedError

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.logger.info("Connected to broker")
            self.connected = True  # Signal connection
        else:
            self.logger.error("Connection to broker failed")

class MQTTSensor:
    def __init__(self, device_id, device, client, master_device):
        self.client = client
        self.master_device = master_device
        self.device = device
        self.device_id = device_id
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.device_id}")
        #self.unit = unit
        self.device_class = device['device_class']
        self.value = None

        self.topic_prefix = f"homeassistant/sensor/{self.master_device['name']}/{self.device_id}"
        self.topics = {
            "state": f"{self.topic_prefix}/state"
        }
        self.config_topic = f"{self.topic_prefix}/config"
        self.set_topic = None
        self.reannounce_after = 15
        self.current_iteration = 0

    def register(self):
        config = {
            "~": self.topic_prefix,
            "name": self.device["name"],
            "state_topic": "~/state",
            "unique_id": self.device_id,
            "device": {
                "manufacturer": self.master_device['manufacturer'],
                "identifiers": self.device_id,
                "name": self.device_id,
                "via_device": self.master_device['identifiers']
            }
        }
        if "unit" in self.device.keys():
             config["unit_of_measurement"] = self.device["unit"]
        if self.device_class != "none":
            config["device_class"] = self.device_class
        self.client.publish(self.config_topic, payload=json.dumps(config), qos=0, retain=True)

    def set(self, value, topic="state"):
        #self.logger.info(f"Got value: {value}")
        if value != self.value or self.current_iteration > self.reannounce_after:
            self.client.publish(f"{self.topics[topic]}", payload=value, qos=0, retain=True)
            self.value = value
            self.current_iteration = 0
        else:
            self.current_iteration += 1
            #self.logger.info(f"Ignoring, iteration {self.current_iteration}")


class MQTTNumber:
    def __init__(self, device_id, device, client, master_device):
        self.client = client
        self.unique_id = f"{device_id}"
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.unique_id}")
        self.device = device
        self.master_device = master_device
        self.value = None
        self.new_value = self.value
        self.topic_prefix = f"homeassistant/number/{master_device['identifiers']}/{self.unique_id}"
        self.set_topic = f"{self.topic_prefix}/set"
        self.status_topic = f"{self.topic_prefix}/state"
        self.reannounce_after = 15
        self.current_iteration = 0

    def register(self):
        config = {
            "~": self.topic_prefix,
            "name": self.device["name"],
            "command_topic": "~/set",
            "state_topic": "~/state",
            "unique_id": self.unique_id,
            "device": {
                "identifiers": self.unique_id,
                "name": self.unique_id,
                "via_device": self.master_device['identifiers']
            },
            "retain": False,
            "step": "0.1"
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)

    def set(self, value, topic="state"):
        # self.logger.info(f"Got value: {value}")
        if value != self.value or self.current_iteration > self.reannounce_after:
            self.client.publish(f"{self.status_topic}", payload=value, qos=0, retain=True)
            self.value = value
            self.current_iteration = 0
        else:
            self.current_iteration += 1
            # self.logger.info(f"Ignoring, iteration {self.current_iteration}")


class MQTTSelect():
    def __init__(self, device_id, device, client, master_device):
        self.client = client
        self.unique_id = f"{device_id}"
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.unique_id}")
        self.device = device
        self.master_device = master_device
        self.value = None
        self.new_value = self.value
        self.topic_prefix = f"homeassistant/select/{master_device['identifiers']}/{self.unique_id}"
        self.set_topic = f"{self.topic_prefix}/set"
        self.status_topic = f"{self.topic_prefix}/state"
        self.reannounce_after = 15
        self.current_iteration = 0

    def register(self):
        config = {
            "~": self.topic_prefix,
            "name": self.device["name"],
            "command_topic": "~/set",
            "state_topic": "~/state",
            "unique_id": self.unique_id,
            "device": {
                "identifiers": self.unique_id,
                "name": self.unique_id,
                "via_device": self.master_device['identifiers']
            },
            "retain": False,
            "options": self.device["select_values"]
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)

    def set(self, value, topic="state"):
        # self.logger.info(f"Got value: {value}")
        if value != self.value or self.current_iteration > self.reannounce_after:
            self.client.publish(f"{self.status_topic}", payload=value, qos=0, retain=True)
            self.value = value
            self.current_iteration = 0
        else:
            self.current_iteration += 1
            # self.logger.info(f"Ignoring, iteration {self.current_iteration}")



class MQTTSwitch():
    def __init__(self, device_id, device, client, master_device):
        self.client = client
        self.unique_id = f"{device_id}"
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.unique_id}")
        self.device = device
        self.master_device = master_device
        self.value = None
        self.new_value = self.value
        self.topic_prefix = f"homeassistant/switch/{master_device['identifiers']}/{self.unique_id}"
        self.set_topic = f"{self.topic_prefix}/set"
        self.status_topic = f"{self.topic_prefix}/state"
        self.reannounce_after = 15
        self.current_iteration = 0

    def register(self):
        config = {
            "~": self.topic_prefix,
            "name": self.device["name"],
            "command_topic": "~/set",
            "state_topic": "~/state",
            "unique_id": self.unique_id,
            "device": {
                "identifiers": self.unique_id,
                "name": self.unique_id,
                "via_device": self.master_device['identifiers']
            },
            "retain": False,
            "payload_on": 1,
            "payload_off": 0
        }
        self.client.publish(f"{self.topic_prefix}/config", payload=json.dumps(config), qos=0, retain=True)

    def set(self, value, topic="state"):
        #self.logger.info(f"Got value: {value}")
        if value != self.value or self.current_iteration > self.reannounce_after:
            self.client.publish(f"{self.status_topic}", payload=value, qos=0, retain=True)
            self.value = value
            self.current_iteration = 0
        else:
            self.current_iteration += 1
            # self.logger.info(f"Ignoring, iteration {self.current_iteration}")

class MQTTThermostat:
    def __init__(self, name, room, client, device, unique_id):
        self.name = name
        self.client = client
        self.device = device
        self.device["identifiers"] = unique_id
        self.unique_id = unique_id
        self.value = None
        self.foxtrot_topic_prefix = f"foxtrot/thermostat/{room}"
        self.registration_topic = f"homeassistant/climate"
        self.reannounce_after = 15
        self.current_iteration = 0
        self.values = {}
        self.register()

    def register(self):
        config = {
            "~": self.foxtrot_topic_prefix,
            "name": self.name,
            "unique_id": self.unique_id,
            "device": self.device,
            "modes": ["auto"],
            #"mode_command_topic": "~/mode/cmd",
            "mode_state_topic": "~/mode/state",
            #"temperature_high_command_topic": "~/temperature_high/cmd",
            #"temperature_low_command_topic": "~/temperature_low/cmd",
            "temperature_high_state_topic": "~/temperature_high/state",
            "temperature_low_state_topic": "~/temperature_low/state",
            "action_topic": "~/action/state",
            #"away_mode_command_topic": "~/away_mode/cmd",
            "away_mode_state_topic": "~/away_mode/state",
            "current_temperature_topic": "~/current_temperature/state",
            #"fan_mode_command_topic" : "",
            #"fan_mode_state_topic": "",
            "fan_modes": [0,1,2,3],
            "precision": 0.1,
            "temperature_unit": "C",
            "temp_step": 0.5
        }
        self.client.publish(f"{self.registration_topic}/{self.name}/config", payload=json.dumps(config), qos=0, retain=True)


class MQTTLight:
    def __init__(self, name, room, client, device):
        self.name = name
        self.client = client
        self.device = device
        self.device["identifiers"] = name
        self.unique_id = name
        self.value = None
        self.foxtrot_topic_prefix = f"foxtrot/light/{room}"
        self.registration_topic = f"homeassistant/light"
        self.reannounce_after = 15
        self.current_iteration = 0
        self.values = {}
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{device}")
        self.register()

    def register(self):
        config = {
            "~": self.foxtrot_topic_prefix,
            "name": self.name,
            "unique_id": self.unique_id,
            "device": self.device,
            "command_topic": "~/cmd",
            "state_topic": "~/state"
        }
        self.client.publish(f"{self.registration_topic}/{self.name}/config", payload=json.dumps(config), qos=0, retain=True)


class MQTTShade:
    def __init__(self, shade_id, shade_config, mqtt_config, rf_config):
        self.shade_id = shade_id
        self.logger = logging.getLogger(f"{self.__class__.__name__}_{self.shade_id}")
        self.logger.info(f"Initializing")
        self.logger.info(shade_config)
        self.shade_config = shade_config
        self.connected = False
        self.mqtt_config = mqtt_config
        self.rf_config = rf_config
        self.client = None
        self.position_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/position"
        self.command_topic = f"{self.mqtt_config['topic_prefix']}/{self.shade_id}/set"
        self.timeout = 1

    def register(self):
        topic = f"homeassistant/cover/shade{self.shade_id}/config"
        payload = {
            "name": f"{self.shade_config['name']}",
            "command_topic": self.command_topic,
            "position_topic": self.position_topic,
            "position_open": self.shade_config['dark'],
            "position_closed": 0,
            "retain": True,
            "optimistic": True,
            "unique_id": f"{self.shade_id}.cover",
            "device": {
                "identifiers": f"rc.{self.shade_id}"
            },
            "device_class": "shade"
        }
        self.logger.info(f"Registering: {self.shade_id}")
        self.client.publish(topic, json.dumps(payload))

