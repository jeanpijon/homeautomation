from telnetlib import Telnet
from multiprocessing import Process, Event, Queue
import logging
from queue import Empty
import yaml

logging.basicConfig(level=logging.INFO)
class Foxtrot(Process):
    def __init__(self, config_path, state_q: Queue, set_q: Queue, event):
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.config = None
        self.controller = None
        self.invalid_value = 51968
        self.baseaddress = 0
        self.unit_address = 60
        self.state_q = state_q
        self.set_q = set_q
        self.devices = {}
        self._stop_event = event
        self.active_vars = {}
        self.initialize(config_path)

    def load_config(self, path):
        with open(path) as c:
            self.config = yaml.load(c, Loader=yaml.FullLoader)

    def get_value(self, var):
        self.controller.write(f"GET:{var}\n".encode())
        line = self.controller.read_until("\r\n".encode()).decode('utf-8')
        cmd, data, value = self.parse_line(line)
        self.logger.info(f"{cmd} {data} {value}")
        return cmd, data, value

    def set_value(self, var, value):
        self.controller.write(f"SET:{var},{value}\n".encode())
        line = self.controller.read_until("\r\n".encode()).decode('utf-8')
        cmd, data, value = self.parse_line(line)
        self.logger.info(f"{cmd} {data} {value}")
        return cmd, data, value

    def enable_var(self, var):
        self.controller.write(f"EN:{var}\n".encode())

    def parse_line(self, line):
        self.logger.info(f"Parsing line: {line}")
        if line.startswith("WARNING"):
            return "Warning", None, None
        cmd, data = line.strip().split(":")
        if len(data) == 0:
            return cmd, None, None
        if cmd == "LIST":
            var_name, var_type = data.split(",")
            return cmd, var_name, var_type
        else:
            var_name, var_value = data.split(",")
            return cmd, var_name, var_value

    def initialize(self, config_path):
        self.logger.info("Initializing")
        self.load_config(config_path)
        self.controller = Telnet(self.config['plccoms']['host'], self.config['plccoms']['port'])
        for device_name, device in self.config['devices'].items():
            for plc_var, var_data in device['vars'].items():
                self.enable_var(plc_var)
                self.active_vars[plc_var] = var_data
        self.enumerate_devices()
        self.logger.info("Initialized")

    def enumerate_devices(self):
        self.controller.write("LIST:\n".encode())
        while True:
            line = self.controller.read_until("\r\n".encode()).decode('utf-8')
            cmd, data, value = self.parse_line(line)
            if data is None:
                break
            self.logger.info(f"{cmd}, {data}")

    def run(self):
        self.logger.info(f"Starting run")
        for var in self.active_vars.keys():
            cmd, var, value = self.get_value(var)
            self.state_q.put((var, value))
        while not self._stop_event.is_set():
            if not self.set_q.empty():
                try:
                    while True:
                        payload = self.set_q.get_nowait()
                        device_name, value = payload
                        self.logger.info(f"Setting {device_name} to {value}")
                        self.set_value(device_name, value)
                except Empty:
                    pass

            while True:
                line = self.controller.read_until("\r\n".encode(), timeout=1).decode('utf-8')
                #FIXME: may be throwing up some data due to race condition
                #(when timeout, then return whatever is in buffer, even incomplete)
                if len(line) > 0:
                    if line.endswith("\r\n"):
                        cmd, device_name, value = self.parse_line(line)
                        self.state_q.put((device_name, value))
                        self.logger.info(f"{device_name}, {value}")
                    else:
                        self.logger.warning(f"Possible data loss, buffer content: {line}")
        return
# #
# ev = Event()
# q1 = Queue()
# q2 = Queue()
# # q2.put(("temperature_water_requested", 48))
# hp = Foxtrot("/opt/homeassistant-dev/services/config/foxtrot/config.yaml", q1, q2, ev)
# hp.start()
#
