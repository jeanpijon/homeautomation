from multiprocessing import Queue, Event

from foxtrot.controller import Foxtrot
from common.mqtt import BaseMQTTAdapter, MQTTNumber, MQTTSwitch, MQTTSelect
from common.mqtt import MQTTSensor


class FoxtrotMQTTAdapter(BaseMQTTAdapter):
    def __init__(self, config_path):
        super().__init__(config_path)
        self.config_path = config_path
        self.run_allowed = True
        self.q = Queue()
        self.control_q = Queue()
        self.event = Event()
        self.devices = {}
        self.master_device = None
        self.initialize()

    def get_mqtt_class(self, mqtt_class):
        if mqtt_class == "sensor":
            return MQTTSensor
        elif mqtt_class == "number":
            return MQTTNumber
        elif mqtt_class == "switch":
            return MQTTSwitch
        elif mqtt_class == "select":
            return MQTTSelect
        else:
            self.logger.warning(f"Unhandled mqtt class: {mqtt_class}")
            return False

    def initialize(self):
        self.master_device = {
            "manufacturer": "TECO",
            "model": "CP-2000",
            "suggested_area": "technicka-mistnost",
            "identifiers": self.config['id'],
            "name": "foxtrot"
        }
        self.controller = Foxtrot(self.config_path, self.q, self.control_q, self.event)

        for device_name, device in self.config['devices'].items():
            mqtt_device_class = self.get_mqtt_class(device['mqtt_class'])
            if mqtt_device_class:
                self.devices[device_name] = {
                    "object": mqtt_device_class(device_name, device, self.client, self.master_device),
                    "vars": {plc_var: var_data["topic"] for plc_var, var_data in device['vars'].items()},
                    "inverse_vars": {var_data["topic"]: plc_var for plc_var, var_data in device['vars'].items()}
                }

                # if self.devices[device_name].set_topic:
                #     self.logger.info(f"Setting callback for {device_name} on {self.devices[device_name].set_topic}")
                #     self.client.message_callback_add(f"{self.devices[device_name].set_topic}", self.set_callback)
        self.connect()
        for device_name, device in self.devices.items():
            device["object"].register()
            # if self.devices[device_name].set_topic:
            #     self.client.subscribe(f"{self.devices[device_name].set_topic}", 0)

    def run(self):
        self.logger.info(f"Starting")
        self.controller.start()
        while self.run_allowed:
            plcc_var_name, value = self.q.get()
            self.logger.info(f"Got PAYLOAD {plcc_var_name}, {value}")
            for device_name, device in self.devices.items():
                #self.logger.info(f"search {device}")
                if plcc_var_name in device["vars"].keys():
                   mqtt_var_name = device["vars"][plcc_var_name]
                   #self.logger.info(f"MQTT name {mqtt_var_name}")
                   device['object'].set(value, mqtt_var_name)

        self.logger.info("Requesting stop on Heat Pump")
        self.event.set()
        self.controller.join()
        self.logger.info("Exiting")

    def stop(self):
        self.logger.info(f"Stop requested")
        self.run_allowed = False

    def set_callback(self, client, userdata, message):
        payload = message.payload.decode("utf-8")
        device_id = message.topic.split('/')[-2]
        self.logger.info(f"Obtained data {payload}, {device_id}")
        self.control_q.put((device_id, payload))

#
# fox = FoxtrotMQTTAdapter("/opt/homeassistant-dev/services/config/foxtrot/config.yaml")
# fox.run()
