import signal
import logging
import argparse
from multiprocessing import Queue

import yaml

from shades.controller import ShadeRFController


def main(args):
    logging.basicConfig(level=logging.DEBUG)

    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)

    signal.signal(signal.SIGINT, original_sigint_handler)
    # read config
    with open(args.config) as c:
        config = yaml.load(c, Loader=yaml.FullLoader)
    # instantiate mqtt bridge instances for shades
    # instantiate RF transmitter
    rx_q = Queue()
    tx_q = Queue()
    controller = ShadeRFController(config, rx_q, tx_q)
    controller.start()

    actions = config['rf']['actions']
    factory_prefix = config['rf']['factory_prefix']
    remoteid = 0b01010011110
    channel = 2

    def get_code(remote_id, channel, action):
        return (factory_prefix << 21) | (int(remote_id) << 10) | (int(channel) << 7) | actions[action]

    tx_q.put((get_code(remoteid, channel, "OPEN")))
    tx_q.put((get_code(remoteid, channel, "OPEN")))
    tx_q.put((get_code(remoteid, channel, "OPEN")))
    tx_q.put((get_code(remoteid, channel, "UP_STOP")))
    tx_q.put((get_code(remoteid, channel, "UP_STOP")))

    # tx_q.put((get_code(remoteid, channel, "CLOSE")))
    # tx_q.put((get_code(remoteid, channel, "CLOSE")))
    # tx_q.put((get_code(remoteid, channel, "CLOSE")))
    # tx_q.put((get_code(remoteid, channel, "DOWN_STOP")))
    # tx_q.put((get_code(remoteid, channel, "DOWN_STOP")))

    # instantiate RF receiver


#if __name__ == "__main__":
parser = argparse.ArgumentParser(description='MQTT bridge for controlling Unnamed Shades')
parser.add_argument('--config', action='store', type=str, default="/opt/homeassistant-dev/services/config/shades/config.yaml")
parser.add_argument('--verbose', action='store', type=int, default=5)

args, unknown = parser.parse_known_args()
main(args)